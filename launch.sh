#!/bin/bash
export CURRENT_FOLDER=$(pwd)
export WORKDIR="/go/src/bitbucket.org/espinpro/brf.im"

function gotest () {
    docker run --rm --name gotest -v ${CURRENT_FOLDER}:${WORKDIR} --workdir ${WORKDIR} golang:1.16-stretch /bin/bash -c "go test -coverprofile=/tmp/coverage.out -cover -timeout 30s -race -p 1 ./...; go tool cover -html=/tmp/coverage.out -o coverage.html; go tool cover -func=/tmp/coverage.out | grep total"
}

function golinter () {
    docker run --rm --name golinter -v ${CURRENT_FOLDER}:${WORKDIR} --workdir ${WORKDIR} golangci/golangci-lint golangci-lint run ./...
}
