package main

import (
	"context"
	"log/slog"

	"go.mongodb.org/mongo-driver/mongo/readpref"

	"bitbucket.org/espinpro/espincore/v3/server/profiler"

	"bitbucket.org/espinpro/espincore/v3/server/instance"

	"bitbucket.org/espinpro/espincore/v3/db/mongodb"

	"bitbucket.org/espinpro/brf.im/handler"

	"github.com/gofiber/fiber/v2"

	"bitbucket.org/espinpro/espincore/v3/app"
	_ "bitbucket.org/espinpro/espincore/v3/fastlog/handlers/stderr"
	"bitbucket.org/espinpro/espincore/v3/fastlog/metrics"
)

func main() {
	ctx := context.Background()

	app.WebMain(ctx, ":8080", "brf.im", func(
		ctx context.Context,
		app *fiber.App,
		met *metrics.OTLPMetric,
	) error {
		mongoClient, err := mongodb.Default()
		if err != nil {
			return err
		}

		hl, err := handler.NewHandler(ctx, app, mongoClient)
		if err != nil {
			return err
		}

		profiler.AddHealthCheck(func() error {
			return mongoClient.Ping(ctx, readpref.SecondaryPreferred())
		})

		slog.Info("Instance ready", "id", instance.GetShortInstanceID())

		return hl.Run()
	})
}
