FROM golang:1.23-alpine as build

WORKDIR /go/src/bitbucket.org/espinpro/brf.im
ADD . .
RUN go build -o /go/bin/server ./cmd/server
CMD ["/go/bin/server"]

FROM alpine:3.7

COPY  --from=build /go/bin/server /go/bin/server
CMD ["/go/bin/server"]
